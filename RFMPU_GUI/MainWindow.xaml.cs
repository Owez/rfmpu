﻿using RFMPU.Utilities;
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;

namespace RFMPU_GUI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly BackgroundWorker worker = new BackgroundWorker();
        private static UtilityFile utilityFile = new UtilityFile();
        private static MainWindow mainWindow = new MainWindow();

        static readonly string downloadURL = "https://www.dropbox.com/s/n3v4o3a6bx0xubd/RFMB6_WINDOWS.zip?dl=1";
        static readonly string zipLocation = string.Format("{0}ToInstall.zip", AppDomain.CurrentDomain.BaseDirectory);
        public static string targetLocation = string.Format("{0}../RFMP_{1}", AppDomain.CurrentDomain.BaseDirectory, UtilityJson.PullDownloadTally());
        private bool hasBeenRan = false;

        public MainWindow()
        {
            InitializeComponent();
        }
        
        private void Worker_DoWork(object sender, DoWorkEventArgs e)
        {
            worker.ReportProgress(0, "Downloading..");
            utilityFile.DownloadFromURL(downloadURL, zipLocation);
            worker.ReportProgress(0, "Unzipping..");
            utilityFile.ExtractZip(zipLocation, targetLocation);
            worker.ReportProgress(0, "Cleaning..");
            utilityFile.DeleteSpecifiedFile(zipLocation); // attempt to delete install zip

            if(UtilityJson.PullShouldAutoDelete())
                utilityFile.DeleteWholeDir(string.Format("{0}RFMP_{1}", AppDomain.CurrentDomain.BaseDirectory, UtilityJson.PullDownloadTally() - 1)); // -2 because it adds just before so it shouldnt delete thing it downloaded and to stay good with cli

            targetLocation = string.Format("{0}RFMP_{1}", AppDomain.CurrentDomain.BaseDirectory, UtilityJson.PullDownloadTally());
            hasBeenRan = false;

            worker.ReportProgress(0, "Idle");
        }

        private void Worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            LbStatus.Content = "Status: " + e.UserState.ToString();

            // make button usable if install finished
            if(e.UserState.ToString().ToLower().StartsWith("idle"))
            {
                BtnStartInstall.IsEnabled = true;
                BtnStartGame.IsEnabled = true;
            }
        }

        private void BtnStartInstall_Click(object sender, RoutedEventArgs e)
        {
            worker.WorkerReportsProgress = true; // allowes it to cheat and change label status
            worker.DoWork += Worker_DoWork; // setting methods up
            worker.ProgressChanged += Worker_ProgressChanged; // setting methods up

            if (!hasBeenRan)
            {
                hasBeenRan = true;
                worker.RunWorkerAsync(); // run background worker (basically start installation)
                UtilityJson.PushPresets();

                // make button not usable if install started
                BtnStartInstall.IsEnabled = false;
                BtnStartGame.IsEnabled = false; // incase of second install
            }
        }

        private void BtnStartGame_Click(object sender, RoutedEventArgs e)
        {
            Process.Start(string.Format("{0}../RFMP_{1}/RFMB6_WINDOWS/RFMB6.exe", AppDomain.CurrentDomain.BaseDirectory, UtilityJson.PullDownloadTally() - 1));
        }
    }
}
