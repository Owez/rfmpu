using System;
using System.IO;
using System.IO.Compression;
using System.Net;
using System.Windows.Forms;

namespace RFMPU.Utilities
{
    class UtilityFile
    {
        public static string RandomFilenameForGUI()
        {
            Random random = new Random();

            return random.Next(1, 9999).ToString();
        }

        public void DownloadFromURL(string downloadLocation, string destinationName)
        {
            using (var client = new WebClient())
            {
                client.DownloadFile(downloadLocation, destinationName);
            }
        }

        public void CreateZip(string fileLocation, string destinationName)
        {
            if(File.Exists(destinationName))
                ZipFile.CreateFromDirectory(fileLocation, destinationName);
        }

        public void ExtractZip(string zipLocation, string extractLocation)
        {
            try
            {
                ZipFile.ExtractToDirectory(zipLocation, extractLocation);
            }
            catch
            {
                Application.Restart();
                Environment.Exit(0);
            }
        }

        public void DeleteSpecifiedFile(string fileToDelete)
        {
            if(File.Exists(fileToDelete))
                File.Delete(fileToDelete);
        }

        public void DeleteWholeDir(string fileToDelete)
        {
            try
            {
                DirectoryInfo di = new DirectoryInfo(fileToDelete);

                foreach (FileInfo file in di.EnumerateFiles())
                {
                    file.Delete();
                }
                foreach (DirectoryInfo dir in di.EnumerateDirectories())
                {
                    dir.Delete(true);
                }
            }
            catch
            {
                UtilityPrint.FullFormat("Looks like RFMPU could not automatically delete the last directory.", false);
            }
        }
    }
}
