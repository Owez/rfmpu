﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.IO;

namespace RFMPU.Utilities
{
    class UtilityJson
    {
        private static readonly string jsonLocation = string.Format("{0}AppConfig.json", AppDomain.CurrentDomain.BaseDirectory);

        #region Pulling
        private static string PullJsonFile()
        {
            using (StreamReader reader = new StreamReader(jsonLocation))
                return JsonConvert.DeserializeObject(reader.ReadToEnd()).ToString();
        }

        public static string PullProjectName()
        {
            dynamic jsonFile = JObject.Parse(PullJsonFile());
            return jsonFile.ProjectName;
        }

        public static int PullDownloadTally()
        {
            dynamic jsonFile = JObject.Parse(PullJsonFile());
            return jsonFile.DownloadTally;
        }

        public static int PullHoursPerDownload()
        {
            dynamic jsonFile = JObject.Parse(PullJsonFile());
            return jsonFile.HoursPerDownload;
        }

        public static bool PullShouldAutoDownload()
        {
            dynamic jsonFile = JObject.Parse(PullJsonFile());
            return jsonFile.ShouldAutoDownload;
        }

        public static bool PullShouldAutoDelete()
        {
            dynamic jsonFile = JObject.Parse(PullJsonFile());
            return jsonFile.ShouldAutoDelete;
        }
        #endregion

        #region Pushing
        private class PushData
        {
            public string ProjectName { get; set; }
            public int DownloadTally { get; set; }
            public int HoursPerDownload { get; set; }
            public bool ShouldAutoDownload { get; set; }
            public bool ShouldAutoDelete { get; set; }
        }

        public static void PushPresets()
        {
            PushData pushPresetsData = new PushData
            {
                ProjectName = PullProjectName(),
                DownloadTally = PullDownloadTally() + 1,
                HoursPerDownload = PullHoursPerDownload(),
                ShouldAutoDownload = PullShouldAutoDownload(),
                ShouldAutoDelete = PullShouldAutoDelete()
            };

            File.WriteAllText(jsonLocation, JsonConvert.SerializeObject(pushPresetsData));
        }
        #endregion
    }
}
