﻿using System;

namespace RFMPU.Utilities
{
    class UtilityPrint
    {
        #region FullFormat
        public static void FullFormat(string text, bool isError)
        {
            if(isError)
                Console.WriteLine(string.Format("{0} [ERROR]: {1}", DateTime.Now.ToString("h:mm:ss"), text));
            else
                Console.WriteLine(string.Format("{0} [WARN]: {1}", DateTime.Now.ToString("h:mm:ss"), text));
        }

        public static void FullFormat(string text)
        {
            Console.WriteLine(string.Format("{0} [OK]: {1}", DateTime.Now.ToString("h:mm:ss"), text));
        }
        #endregion

        #region TimeFormat
        public static void TimeFormat(string text)
        {
            Console.WriteLine(string.Format("{0}: {1}", DateTime.Now.ToString("h:mm:ss"), text));
        }
        #endregion
    }
}
