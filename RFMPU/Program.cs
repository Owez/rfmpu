using RFMPU.Utilities;
using System;
using System.Diagnostics;

namespace RFMPU
{
    class Program
    {
        private static UtilityFile utilityFile = new UtilityFile();

        static readonly string downloadURL = utilityFile.GetDownloadURL();
        static readonly string zipLocation = string.Format("{0}ToInstall.zip", AppDomain.CurrentDomain.BaseDirectory);
        public static string targetLocation = string.Format("{0}../RFMP_{1}", AppDomain.CurrentDomain.BaseDirectory, UtilityJson.PullDownloadTally());

        static void Main(string[] args)
        {
            Console.WriteLine("RFMPU has started!");

            InstallGame(); // always install game once no matter is auto-download is disabled or not

            if(UtilityJson.PullShouldAutoDownload())
                InstallTimer(UtilityJson.PullHoursPerDownload());
        }

        static void InstallTimer(int hoursToWait)
        {
            var whenToTrigger = DateTime.Now.AddHours(hoursToWait);

            while (true)
            {
                if(whenToTrigger <= DateTime.Now) // the "<=" for when program misses a tick
                {
                    UtilityPrint.TimeFormat(string.Format("Attempting to install game, this is installation #{0}!", UtilityJson.PullDownloadTally()));

                    try
                    {
                        InstallGame();
                    }
                    catch (Exception ex)
                    {
                        UtilityPrint.FullFormat(string.Format("Could not install game! Exeption message: {0}.", ex), true);
                    }

                    whenToTrigger = DateTime.Now.AddHours(hoursToWait);
                }
            }
        }

        public static void InstallGame()
        {
            Stopwatch eclapsedTime = Stopwatch.StartNew();

            UtilityPrint.FullFormat("Downloading..");

            try
            {
                utilityFile.DownloadFromURL(downloadURL, zipLocation);
            }
            catch
            {
                UtilityPrint.FullFormat("The RFMP download could not be completed due to it timing out! This is most likely caused by a network error. Please restart RFMPU manually");
            }

            UtilityPrint.FullFormat("Unzipping..");
            utilityFile.ExtractZip(zipLocation, targetLocation);
            UtilityPrint.FullFormat(string.Format("Cleaning ZIP & attempting to delete RFMP_{0}'s contents..", UtilityJson.PullDownloadTally() - 1));
            utilityFile.DeleteSpecifiedFile(zipLocation); // attempt to delete install zip

            if(UtilityJson.PullShouldAutoDelete())
                utilityFile.DeleteWholeDir(string.Format("{0}../RFMP_{1}", AppDomain.CurrentDomain.BaseDirectory, UtilityJson.PullDownloadTally() - 1)); // attempt to delete previous version of RFMP

            UtilityPrint.FullFormat("Saving data to JSON..");
            UtilityJson.PushPresets();

            eclapsedTime.Stop();

            UtilityPrint.FullFormat(string.Format("Processed finished in {0} seconds!", eclapsedTime.ElapsedMilliseconds / 1000));
        }
    }
}