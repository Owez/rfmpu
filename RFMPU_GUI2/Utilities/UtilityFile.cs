using System;
using System.IO;
using System.IO.Compression;
using System.Net;

namespace RFMPU.Utilities
{
    class UtilityFile
    {
        public string RandomFilenameForGUI()
        {
            Random random = new Random();
            return random.Next(1, 9999).ToString();
        }

        public void DownloadFromURL(string downloadLocation, string destinationName)
        {
            using (var client = new WebClient())
            {
                client.DownloadFile(downloadLocation, destinationName);
            }
        }

        public string GetDownloadURL()
        {
            OperatingSystem os = Environment.OSVersion;
            PlatformID pid = os.Platform;
            switch (pid)
            {
                case PlatformID.MacOSX: // mac
                    return "https://www.dropbox.com/s/pkic2u3it5u8yeh/RFMB6_MAC.zip?dl=1";
                case PlatformID.Unix: // linux
                    return "https://www.dropbox.com/s/aay3gtdsercihtm/RFMP_Linuxy.zip?dl=1";
                case PlatformID.Win32NT: // windows
                    return "https://www.dropbox.com/s/n3v4o3a6bx0xubd/RFMB6_WINDOWS.zip?dl=1";
                default: // unrecognised
                    return "https://ufile.io/pgyzt";
            }
        }

        public void CreateZip(string fileLocation, string destinationName)
        {
            ZipFile.CreateFromDirectory(fileLocation, destinationName);
        }

        public void ExtractZip(string zipLocation, string extractLocation)
        {
            ZipFile.ExtractToDirectory(zipLocation, extractLocation);
        }

        public void DeleteSpecifiedFile(string fileToDelete)
        {
            File.Delete(fileToDelete);
        }

        public void DeleteWholeDir(string fileToDelete)
        {
            DirectoryInfo di = new DirectoryInfo(fileToDelete);

            foreach (FileInfo file in di.EnumerateFiles())
            {
                file.Delete();
            }
            foreach (DirectoryInfo dir in di.EnumerateDirectories())
            {
                dir.Delete(true);
            }
        }
    }
}
