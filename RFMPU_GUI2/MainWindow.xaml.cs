﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace RFMPU_GUI2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        #region Changing gui look installing
        public void GeneralLook(bool shouldButtonBeUsable)
        {
            if(shouldButtonBeUsable)
            {
                BtnStartInstall.IsEnabled = true;
                PtcLeft.Opacity = 1.0;
                PtcMiddle.Opacity = 1.0;
                PtcRight.Opacity = 1.0;
            }
            else
                BtnStartInstall.IsEnabled = false;
        }

        public void DownloadLook()
        {
            PtcLeft.Opacity = 1.0f;
            PtcMiddle.Opacity = 0.5f;
            PtcRight.Opacity = 0.5f;
        }

        public void InstallLook()
        {
            PtcLeft.Opacity = 0.5;
            PtcMiddle.Opacity = 1.0;
            PtcRight.Opacity = 0.5;
        }

        public void CleanupLook()
        {
            PtcLeft.Opacity = 0.5;
            PtcMiddle.Opacity = 0.5;
            PtcRight.Opacity = 1.0;
        }
        #endregion

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            DownloadManager.InstallGame();
            BtnStartInstall.Content = ". . . . .";
        }
    }
}
