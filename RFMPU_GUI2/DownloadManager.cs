﻿using System;
using System.ComponentModel;
using RFMPU.Utilities;

namespace RFMPU_GUI2
{
    class DownloadManager
    {
        private static BackgroundWorker worker = new BackgroundWorker();
        private static MainWindow mainWindow = new MainWindow();

        public static bool hasBeenRan = false;

        public static void InstallGame()
        {
            Console.WriteLine("Checking if program is in progress");
            mainWindow.BtnStartInstall.Content = "Start Installation";

            if (!hasBeenRan)
            {
                // background worker
                Console.WriteLine("Background worker starting..");

                worker.WorkerReportsProgress = true;
                worker.DoWork += Worker_DoWork;
                worker.ProgressChanged += Worker_ProgressChanged;

                worker.RunWorkerAsync();

                // make button inoprable
                mainWindow.GeneralLook(false);
            }
            else
                Console.WriteLine("Background worker is not starting as one is already in progress.");
        }

        private static void Worker_DoWork(object sender, DoWorkEventArgs e)
        {
            Console.WriteLine("Background worker started!");

            // declaration
            var utilityFile = new UtilityFile();
            string downloadURL = utilityFile.GetDownloadURL();
            string zipLocation = "ToInstall.zip";
            string targetLocation = string.Format("RFMPU_{0}", utilityFile.RandomFilenameForGUI());

            // to keep gui in shape
            hasBeenRan = true;

            // download
            try
            {
                mainWindow.DownloadLook();
                utilityFile.DownloadFromURL(downloadURL, zipLocation);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex); // replace
            }

            // install
            try
            {
                mainWindow.InstallLook();
                utilityFile.ExtractZip(zipLocation, targetLocation);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex); // replace
            }

            // clean
            try
            {
                mainWindow.CleanupLook();
                utilityFile.DeleteSpecifiedFile(zipLocation); // attempt to delete install zip
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex); // replace
            }

            // to keep gui in shape
            hasBeenRan = false;
            mainWindow.GeneralLook(true);
            mainWindow.BtnStartInstall.Content = "Start Installation"; // fix this

            // end of worker
            Console.WriteLine("Background worker finished!");
        }

        private static void Worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {

        }
    }
}
